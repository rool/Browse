/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    Meta.h                                            */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Handling META tags.                               */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 25-Jul-1997 (ADH): Created.                       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_Meta__
  #define Browser_Meta__

  #include <kernel.h>
  #include <tboxlibs/wimp.h>
  #include <tboxlibs/toolbox.h>
  #include <HTMLLib/HTMLLib.h>

  #include "Global.h"

  _kernel_oserror * meta_process_tag    (browser_data * b, HStream * t);
  int               meta_check_refresh  (int eventcode, WimpPollBlock * b, IdBlock * idb, browser_data * handle);
  void              meta_cancel_refresh (browser_data * b);

#endif /* Browser_Meta__ */
