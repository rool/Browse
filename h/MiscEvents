/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    MiscEvents.h                                      */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Miscellaneous event codes used in the Res file.   */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 18-Sep-1997 (ADH): Created from TBevents.h.       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_MiscEvents__
  #define Browser_MiscEvents__

  #define EOpenNewWindow                  0x20000 /* Open a new browser window (usually from icon bar) */
  #define ECloseWindow                    0x20001 /* Close browser window (via. keyboard shortcut)     */
  #define EScrollWindow                   0x20002 /* MayScroll marker clicked on                       */

  /* If Limits_Markers is greater than 0x80 these numberspaces will */
  /* overlap (So Don't Do It!).                                     */

  #define ESetMarkerBase                  0x20100 /* Set a marker   */
  #define EJumpToMarkerBase               0x20180 /* Jump to marker */
  #define EClearMarkerBase                0x20200 /* Clear a marker */

  #define ESetMarkerLimit                 ESetMarkerBase    + Limits_Markers - 1
  #define EJumpToMarkerLimit              EJumpToMarkerBase + Limits_Markers - 1
  #define EClearMarkerLimit               EClearMarkerBase  + Limits_Markers - 1

#endif /* Browser_MiscEvents__ */
