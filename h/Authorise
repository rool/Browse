/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    Authorise.h                                       */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Browser remote authorisation services.            */
/*                                                            */
/* Author:  A.D.Hodgkinson.                                   */
/*                                                            */
/* History: 24-Apr-1997 (ADH): Created.                       */
/*          25-Aug-1997 (ADH): Definitions imported from      */
/*                             TBEvents.h.                    */
/*          10-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_Authorise__
  #define Browser_Authorise__

  #include <kernel.h>
  #include <tboxlibs/toolbox.h>

  /* Authorise dialogue component IDs */

  #define AuthPrompt                      0x01000
  #define AuthUserWrit                    0x01001
  #define AuthUserLabel                   0x01002
  #define AuthPassWrit                    0x01003
  #define AuthPassLabel                   0x01004
  #define AuthCancel                      0x01005
  #define AuthAuthorise                   0x01006

  /* Authorise dialogue event codes */

  #define EAuthCancel                     0x01005
  #define EAuthAuthorise                  0x01006

  /* Function prototypes */

  _kernel_oserror * authorise_create_dialogue    (void * handle, ObjectId * id);
  ObjectId          authorise_return_dialogue_id (void);
  int               authorise_authorise          (int eventcode, ToolboxEvent * event, IdBlock * idb, void * handle);
  int               authorise_cancel             (int eventcode, ToolboxEvent * event, IdBlock * idb, void * handle);

  char            * authorise_read_realm         (char * header_entry);

  int               authorise_find_offset        (char * host, char * realm);
  int               authorise_find_user_name     (char * host, char * realm);
  int               authorise_find_password      (char * host, char * realm);

  _kernel_oserror * authorise_remember           (char * host, char * realm, char * username, char * password);
  void              authorise_forget             (char * host, char * realm);

#endif /* Browser_Authorise__ */
