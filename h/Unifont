/* Copyright 1997 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**************************************************************/
/* File:    Unifont.h                                         */
/*          (C) 2000 Pace Micro Technology PLC                */
/*          All rights reserved                               */
/*                                                            */
/* Purpose: Routines to output ISO10646 characters using      */
/*          UTF-8.                                            */
/*                                                            */
/* Author:  K.J.Bracey.                                       */
/*                                                            */
/* History: 20-Aug-1997 (KJB): Created.                       */
/*          07-Apr-2000 (ADH): 64-wide comments adopted.      */
/**************************************************************/

#ifndef Browser_Unifont__
  #define Browser_Unifont__

  #include <kernel.h>
  #include <Unicode/utf8.h>

  /* Useful definitions */

  #define Service_International_DefineUCS 7

  #define Unifont_WideBitmapLocation      "Unicode:Widefont"
  #define Unifont_WideBitmapSpriteName    "bmp"

  /* Function prototypes */

  void              unifont_start_redraw (void);
  _kernel_oserror * unifont_write        (const char * restrict s, size_t numchars, int x, int y, int xs, int ys);
  _kernel_oserror * unifont_end_redraw   (void);
  int               unifont_widechar     (UCS4 c);

#endif /* Browser_Unifont__ */
